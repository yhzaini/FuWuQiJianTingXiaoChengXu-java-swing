package com.sckj.base;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Blob;
import java.util.Date;

import javax.sql.rowset.serial.SerialBlob;

import org.json.simple.JSONObject;

import com.sckj.util.CommonUtil;
import com.sckj.util.PublicConstant;

/**
 * 所有对象的基类
 * 
 * @author cr
 *
 */
public class BaseObject {
	/**
	 * 通过实体类对象获取json对象
	 * 
	 * @param obj
	 *            实体类对象
	 * @param T
	 *            实体类
	 * @return json对象
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public JSONObject getJsonByObject(Object obj, Class T) throws Exception {
		JSONObject resoult = null; // 返回对象
		Object fieldValue; // 对象属性值
		PropertyDescriptor propertyDescriptor;
		Field[] fields; // 对象属性集合
		Method method; // 对象方法
		Class type; // 对象属性类型

		try {
			if(obj==null){
				return null;
			}
			// 实例化一个对象
			resoult = new JSONObject();
			// 获取对象的属性集合和非用户声明的属性名称集合
			fields = T.getDeclaredFields();
			// 循环属性集合
			for (Field field : fields) {
				if(field.getModifiers()==25){	//排除修饰符为public static final的属性
					continue;
				}
				// 获取当前属性的类型和设置属性get方法的名称
				// System.out.println("field.getName()==" + field.getName());
				type = field.getType();
				propertyDescriptor = new PropertyDescriptor(field.getName(), T);
				// 获取方法和属性值
				method = propertyDescriptor.getReadMethod();
				fieldValue = method.invoke(obj);
				// 判断属性值是否存在
				if (fieldValue != null) {
					if (type.getName().equals("java.util.Date")) {
						// 根据属性类型设置属性值
						resoult.put(field.getName(),
								CommonUtil.dateToString((Date) fieldValue, PublicConstant.DATE_FORMAT_STRING_DEFAULT));
					}else if (type.getName().equals("java.sql.Blob")) {
						Blob tempBlob = (Blob)fieldValue;
						resoult.put(field.getName(), new String((tempBlob.getBytes(1, (int)tempBlob.length()))));
					} else {
						// 根据属性类型设置属性值
						resoult.put(field.getName(), fieldValue);
					}
				}
			}
		} catch (Exception e) {
			// 发生异常后设置返回对象为空并抛出异常
			resoult = null;
			throw e;
		}

		return resoult;
	}

	/**
	 * 通过json对象获取实体类对象
	 * 
	 * @param value
	 *            json对象
	 * @param T
	 *            实体类
	 * @return 实体类对象
	 */
	public Object getObjectByJson(JSONObject value, Class T) throws Exception {
		Object resoult = null; // 返回对象
		Object fieldValue; // 对象属性值
		PropertyDescriptor propertyDescriptor;
		Field[] fields; // 对象属性集合
		Method method; // 对象方法
		Class<?> type; // 对象属性类型
		Class<?> newClass;

		try {
			if(value==null){
				return null;
			}
			// 实例化一个对象
			newClass = Class.forName(T.getName());
			resoult = newClass.newInstance();
			// 获取对象的属性集合和非用户声明的属性名称集合
			fields = T.getDeclaredFields();
			// 循环属性集合
			for (Field field : fields) {
				if(field.getModifiers()==25){	//排除修饰符为public static final的属性
					continue;
				}
				propertyDescriptor = new PropertyDescriptor(field.getName(), T);
				method = propertyDescriptor.getWriteMethod();
				fieldValue = value.get(field.getName());
				if (fieldValue != null && fieldValue.toString().length() > 0) {
					type = field.getType();
					if (type.getName().equals("java.lang.Double")) {
						method.invoke(resoult, Double.parseDouble(fieldValue.toString()));
					} else if (type.getName().equals("java.util.Date")) {
						method.invoke(resoult, CommonUtil.stringToDate(fieldValue.toString(),
								PublicConstant.DATE_FORMAT_STRING_DEFAULT));
					} else if (type.getName().equals("java.sql.Blob")) {
						method.invoke(resoult, new SerialBlob(fieldValue.toString().getBytes()));
					} else if (type.getName().equals("java.lang.Integer")) {
						method.invoke(resoult, Integer.parseInt(fieldValue.toString()));
					} else if (type.getName().equals("java.lang.Float")) {
						method.invoke(resoult, Float.parseFloat(fieldValue.toString()));
					} else {
						method.invoke(resoult, fieldValue);
					}

				}
			}
		} catch (Exception e) {
			// 发生异常后设置返回对象为空并抛出异常
			resoult = null;
			throw e;
		}

		return resoult;
	}
	
	/**
	 * 获取服务器时间的字符串（格式根据参数）
	 * @param format 格式化样式
	 * @return
	 * @throws Exception 
	 */
	public String getServerDateTimeString(String format) throws Exception{
		String retStr = null;	// 返回值
		
		try{
			retStr = CommonUtil.dateToString(new Date(), format);
		}catch(Exception e){
			throw e;
		}
		
		return retStr;
	}
}
