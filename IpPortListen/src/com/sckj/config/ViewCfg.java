package com.sckj.config;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JPanel;

/**
 * 公共参数类
 * @author DQ
 *
 */
public class ViewCfg {
	//public static final String SOCKETIP ="192.168.2.120";
	public static final String SOCKETIP ="127.0.0.1";
	public static final int WIDTH =Toolkit.getDefaultToolkit().getScreenSize().width;
	public static final int HEIGHT =Toolkit.getDefaultToolkit().getScreenSize().height; // 当前页面分辨率(高度)
	
	public static final Font basicFont=new Font("微软雅黑",1,20);
	public static final Font font20=new Font("微软雅黑",0,20);
	public static final Font font25=new Font("微软雅黑",1,25);
	public static final Font font40b=new Font("微软雅黑",1,40);
	
	public static int manageCount=0;//后台操作点击数
	
	public static JPanel content=null;//内容视图
	
	public static int nowPriceZB=0;//当前用户可用金额纸币 (分)
	public static int nowPriceYB=0;//当前用户可用金额硬币
	public static int nowTotalPrice=0;//当前用户可用总金额
	public static int oldTotalPrice=0;//当前用户可用总金额
	
	public static int RESULTTime=1000;//Socket回复时间
	public static int CHSTATE=-1;//出货状态
	public static int YBCOUNT=-1;//硬币数
	
	public static String KEYSTRING="";//键盘按键内容
	
	public static int PageIndex = 1;// 货物列表当前显示页数页数
}
