package com.sckj.config;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.sckj.util.PropertiesUtil;

/**
 * 系统配置
 * 
 * @author yanghz
 *
 */
public class SysConfig {

	private SysConfig() {
	}

	static Logger logger = Logger.getLogger(SysConfig.class);
	/**
	 * 监听ip端口集合
	 */
	public static String LISTEN_IP_PORT_LIST="";
	public static Long LISTEN_SLEEP_TIME=0l;	//分钟

	/**
	 * 系统基础配置
	 */
	public static String HSFGSE_APP_SERVER_SOCKET_HOST = ""; // 应用服务器socket通信ip地址
	public static int HSFGSE_APP_SERVER_SOCKET_PORT; // 应用服务器socket通信端口
	public static String HSFGSE_EXTERNAL_SERVER_URL = ""; // 调度服务器通信地址
	public static String HSFGSE_EXTERNAL_SERVER_CONTENT_TYPE = ""; // 调度服务器接口内容类型
	public static String HSFGSE_UPLOAD_FILE_ROOT_PATH = ""; // 上传文件根路径
	
	public static String DEFAULT_FACE_PHOTO_PATH = ""; // 默认上传目标人脸照片路径
	public static String DEFAULT_FACE_PHOTO_EXPANDED_NAME = ""; // 默认上传目标人脸照片后缀名称
	
	public static String DEFAULT_PIC_URL="";	// 默认图片未找到或丢失
	
	/**
	 * 比对返回分值增加值百分比（0-100之间，0表示无增加值）
	 */
	public static String EXTERNAL_RESOULT_SCORE_PARAM = "";
	/**
	 * 比对返回分值最大值（0-100之间）
	 */
	public static String EXTERNAL_RESOULT_SCORE_MAX = "";
	
	//ftp通信配置
	/**
	 * FTP通信ip
	 */
	public static String HSFGSE_FTP_HOST="";
	/**
	 * FTP通信端口
	 */
	public static int HSFGSE_FTP_PORT=0;
	/**
	 * FTP通信账号
	 */
	public static String HSFGSE_FTP_USERNAME="";
	/**
	 * FTP通信密码
	 */
	public static String HSFGSE_FTP_PASSWORD="";
	/**
	 * FTP下载本地文件
	 */
	public static String HSFGSE_FTP_LOCALDIR="";
	/**
	 * 摄像头抓取照片最后时间
	 */
	public static String MONITOR_TIME="";
	/**
	 * 抓取照片下载休眠时间（单位：分钟）
	 */
	public static int MONITOR_SLEEP_TIME=0;
	/**
	 * 加载配置文件
	 */
	static {
		try {
			Properties properties = PropertiesUtil.read("SysConfig.properties");
			if (properties != null) {
				{
					LISTEN_IP_PORT_LIST=properties.getProperty("listen_ip_port_list");
					LISTEN_SLEEP_TIME=Long.parseLong(properties.getProperty("listen_sleep_time"));
				}
				// 系统基础配置
				{
					HSFGSE_APP_SERVER_SOCKET_HOST = properties.getProperty("hsfgse_app_server_socket_host");
					HSFGSE_APP_SERVER_SOCKET_PORT = Integer
							.parseInt(properties.getProperty("hsfgse_app_server_socket_port"));
					HSFGSE_EXTERNAL_SERVER_URL = properties.getProperty("hsfgse_external_server_url");
					HSFGSE_EXTERNAL_SERVER_CONTENT_TYPE = properties.getProperty("hsfgse_external_server_content_type");
					HSFGSE_UPLOAD_FILE_ROOT_PATH=properties.getProperty("hsfgse_upload_file_root_path");
					DEFAULT_PIC_URL=properties.getProperty("hsfgse_default_pic_url");
				}
				{
					DEFAULT_FACE_PHOTO_PATH=properties.getProperty("default_face_photo_path");
					DEFAULT_FACE_PHOTO_EXPANDED_NAME=properties.getProperty("default_face_photo_expanded_name");
				}
				//ftp通信配置
				{
					HSFGSE_FTP_HOST=properties.getProperty("hsfgse_ftp_host");
					HSFGSE_FTP_PORT=Integer
							.parseInt(properties.getProperty("hsfgse_ftp_port"));
					HSFGSE_FTP_USERNAME=properties.getProperty("hsfgse_ftp_userName");
					HSFGSE_FTP_PASSWORD=properties.getProperty("hsfgse_ftp_password");
					HSFGSE_FTP_LOCALDIR=properties.getProperty("hsfgse_ftp_localDir");
				}
				//临时功能
				{
					MONITOR_TIME=properties.getProperty("monitor_time");
					MONITOR_SLEEP_TIME=Integer.parseInt(properties.getProperty("monitor_sleep_time"));
				}
				// 比对返回分值配置
				{

					EXTERNAL_RESOULT_SCORE_PARAM=properties.getProperty("hsfgse_external_resoult_score_param");
					EXTERNAL_RESOULT_SCORE_MAX=properties.getProperty("hsfgse_external_resoult_score_max");
				}
			}
		} catch (Exception e) {
			logger.error("加载SysProperties异常!", e);
		}
	}
}
