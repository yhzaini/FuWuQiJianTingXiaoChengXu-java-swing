package com.sckj.util;

public class PublicConstant {
	
	
	
	/**
     * 默认日期格式（yyyy-MM-dd hh:mm:ss）
     */
    public static final String DATE_FORMAT_STRING_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    /**
     * 日期格式（yyyyMMddhhmmss）
     */
    public static final String DATE_FORMAT_STRING_1 = "yyyyMMddHHmmss";
    /**
     * 日期格式（yyyy-MM-dd）
     */
    public static final String DATE_FORMAT_STRING_2 = "yyyy-MM-dd";
    /**
     * 默认开始通信协议
     */
    public static final String DEFAULT_SOCKET_BEGIN="socket_begin";
    
    
    /**
     * 默认结束通信协议
     */
    public static final String DEFAULT_SOCKET_END="socket_end";
    /**
     * 默认返回通信协议成功
     */
    public static final String DEFAULT_SOCKET_RESULT_OK="OK";
    /**
     * 默认返回通信协议失败
     */
    public static final String DEFAULT_SOCKET_RESULT_FAIL="FAIL";

	/**
     * 返回代码信息-0：成功
     */
    public static final String RET_CODE_SUCCESS = "0";
	/**
     * 返回代码信息-1：失败
     */
    public static final String RET_CODE_FAIL = "1";
	/**
     * 返回代码信息--1：系统异常
     */
    public static final String RET_CODE_ERR = "-1";
	/**
     * 返回代码信息-2：无数据
     */
    public static final String RET_CODE_NOTHING = "2";
	/**
     * 返回代码信息-3：存在级联
     */
    public static final String RET_CODE_CASCADE = "3";

    /**
     * 日志操作内容-0（调用方法+方法名）
     */
    public static final String LOG_MESSAGE_0 = "\u8c03\u7528\u65b9\u6cd5";
    /**
     * 日志操作内容-0（参数）
     */
    public static final String LOG_MESSAGE_1 = "\u53c2\u6570";
    
    /**
     * 通信参数-方法名
     */
    public static final String PARAMS_ACTION_NAME = "ACTION_NAME";
    /**
     * 通信参数-参数
     */
    public static final String PARAMS_PARAM = "PARAM";
    /**
     * 通信参数-参数集合
     */
    public static final String PARAMS_PARAMS = "PARAMS";
    /**
     * 通信参数-告警参数资源库集合
     */
    public static final String PARAMS_LIBRARY_INFO_LIST = "LIBRARY_INFO_LIST";
    /**
     * 通信参数-库id参数
     */
    public static final String PARAMS_LIBRARY_ID = "LIBRARY_ID";
    /**
     * 通信参数-1:1照片比对第一张
     */
    public static final String PARAMS_PHOTO_ONE = "PHOTO_ONE";
    /**
     * 通信参数-1:1照片比对第二张
     */
    public static final String PARAMS_PHOTO_TWO = "PHOTO_TWO";
    /**
     * 通信参数-返回代码
     */
    public static final String PARAMS_RET_CODE = "RET_CODE";    

    /**
     * 通信参数-返回结果集（查询）
     */
    public static final String PARAMS_RET_LIST = "RET_LIST";
    /**
     * 通信参数-返回结果消息
     */
    public static final String PARAMS_RET_MESSAGE = "RET_MESSAGE";

    /**
     * 通信参数-分页对象
     */
    public static final String PARAMS_PAGER = "PAGER";    
    /**
     * 通信参数-登录对象
     */
    public static final String PARAMS_USER = "USER";
    
    /**
     * SQL参数-结果记录总数
     */
    public static final String SQL_QUERY_COUNT = "COUNTNUM";
    /**
     * 默认分页规格
     */
    public static final Integer DEFAULT_PAGE_SIZE=15;
    
    /**
     * 调度服务器接口调用方式-POST
     */
    public static final String EXTERNAL_SERVER_METHOD_POST="POST";
    
    /**
     * 数据库序列名称-目标人脸照片编号序列
     */
    public static final String SEQ_FACE_PHOTO_CODE="SEQ_FACE_PHOTO_CODE";
    /**
     * 数据库序列名称-资源库编号序列
     */
    public static final String SEQ_LIBRARY_INFO_CODE="SEQ_LIBRARY_INFO_CODE";
    /**
     * 数据库序列名称-监控区域编号序列
     */
    public static final String SEQ_MONITOR_AREA_CODE="SEQ_MONITOR_AREA_CODE";
    /**
     * 数据库序列名称-监控位置编号序列
     */
    public static final String SEQ_MONITOR_LOCATION_CODE="SEQ_MONITOR_LOCATION_CODE";
}
