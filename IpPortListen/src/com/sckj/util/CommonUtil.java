package com.sckj.util;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CommonUtil {
	/**
	 * 将字符串对象转换为Date对象
	 * 
	 * @param timestamp
	 *            日期字符串
	 * @param formatString
	 *            日期格式化字符串
	 * @return
	 * @throws Exception
	 */
	public static Date stringToDate(String timestamp, String formatString)
			throws Exception {
		SimpleDateFormat formater;
		Date resoult = null;
		try {
			if (timestamp != null && !timestamp.equals("")) {
				formater = new SimpleDateFormat(formatString);
				resoult = formater.parse(timestamp);
			}
		} catch (Exception e) {
			throw e;
		}
		return resoult;
	}

	/**
	 * 将Date对象转换为字符串对象
	 * 
	 * @param timestamp
	 *            日期字符串
	 * @param formatString
	 *            日期格式化字符串
	 * @return
	 * @throws Exception
	 */
	public static String dateToString(Date timestamp, String formatString){
		SimpleDateFormat formater;
		String resoult = "";

		try {
			if (timestamp != null) {
				formater = new SimpleDateFormat(formatString);
				resoult = formater.format(timestamp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resoult;
	}

	

	/**
	 * 获取唯一编码（去掉“-”，剩余32位）
	 * 
	 * @throws Exception
	 */
	public static String getTbUUID() throws Exception {
		UUID newId; // 唯一编码对象
		String resoultStr; // 返回字符串对象

		try {
			// 获取唯一编码
			newId = UUID.randomUUID();
			// 去掉“-”
			resoultStr = newId.toString().replace("-", "");
		} catch (Exception e) {
			// 出现异常后抛出异常
			throw e;
		}
		// 返回
		return resoultStr;
	}
	
	/**
	 * 大写字母转换为下划线加该字母。例：字符串‘TbUnregistWare’转换后为‘TB_UNREGIST_WARE’
	 * @param value
	 * @return
	 * @throws Exception 
	 */
	public static String uperCaseToUnderline(String value) throws Exception{
		String resoultStr = ""; // 返回字符串对象

		try {
			// 获取唯一编码
			if(value != null && value.length() > 0){
				resoultStr += value.charAt(0);
				for(int i = 1; i < value.length(); i++){
					if(Character.isUpperCase(value.charAt(i)) == true){
						resoultStr += "_" + value.charAt(i);
					}else{
						resoultStr += value.charAt(i);
					}
				}
				resoultStr = resoultStr.toUpperCase();
			}
		} catch (Exception e) {
			// 出现异常后抛出异常
			throw e;
		}
		// 返回
		return resoultStr;
	}
	
	/**
	 * 获取最后一个小数点后面的字符串（可用于获取文件后缀名）
	 * @param value	要转换的字符串
	 * @return
	 * @throws Exception 
	 */
	public static String getPointString(String value) throws Exception{
		String resoultStr = ""; // 返回字符串对象

		try {
			if(value.length() > value.lastIndexOf(".")){
				resoultStr = value.substring(value.lastIndexOf(".") + 1);
			}			
		} catch (Exception e) {
			// 出现异常后抛出异常
			throw e;
		}
		// 返回
		return resoultStr;
	}
	
	/**
	 * 在字符串左边补零
	 * @param value	要补零的字符串对象
	 * @param length	最终获得的字符串的长度
	 * @return
	 * @throws Exception 
	 */
	public static String fillLeftZero(String value, int length) throws Exception{
		String resoultStr = ""; // 返回字符串对象

		try {
			if(value.length() >= length){
				resoultStr = value;
			}else{
				for(int i = 0;i<length-value.length();i++){
					resoultStr += "0";
				}
				resoultStr += value;
			}
		} catch (Exception e) {
			// 出现异常后抛出异常
			throw e;
		}
		// 返回
		return resoultStr;
	}
	/**
	 * 反射排除特定字段
	 * @param fields
	 * @return
	 */
	public static Field[] getFilterField(Field[] fields){
		List<Field> list=null;//集合	
		List<Field> tempList=null;//移除元素后的集合
		try {
			if(fields!=null && fields.length>=0){
				//数组转换集合
				list=Arrays.asList(fields);
				tempList=new ArrayList<Field>();
				for (Field field : list) {
					if(field.getName().equals("beginDate")){
						continue;
					}
					if(field.getName().equals("endDate")){
						continue;
					}
					tempList.add(field);
				}
				fields=tempList.toArray(new Field[1]);//返回一个包含所有对象的指定类型的数组 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return fields;
	}
	
	/** 
	 * 字符串转换unicode 
	 */  
	public static String stringToUnicode(String string) {  
	   
	    StringBuffer unicode = new StringBuffer();  
	   
	    for (int i = 0; i < string.length(); i++) {  
	   
	        // 取出每一个字符  
	        char c = string.charAt(i);  
	   
	        // 转换为unicode  
	        unicode.append("\\u" + Integer.toHexString(c));  
	    }  
	   
	    return unicode.toString();  
	}
	
	/** 
	 * unicode 转字符串 
	 */  
	public static String unicodeToString(String unicode) {  
	   
	    StringBuffer string = new StringBuffer();  
	   
	    String[] hex = unicode.split("\\\\u");  
	   
	    for (int i = 1; i < hex.length; i++) {  
	   
	        // 转换出每一个代码点  
	        int data = Integer.parseInt(hex[i], 16);  
	   
	        // 追加成string  
	        string.append((char) data);  
	    }  
	   
	    return string.toString();  
	} 
	
	
}
