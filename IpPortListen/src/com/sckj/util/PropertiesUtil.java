package com.sckj.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 配置文件读取处理包
 * 
 * @author yanghz
 *
 */
public class PropertiesUtil {

	static Logger logger = Logger.getLogger(PropertiesUtil.class);

	private PropertiesUtil() {
	}

	/**
	 * 读取Properties
	 * 
	 * @param fileName
	 * @return Properties
	 */
	public static Properties read(String fileName) {
		try {
			if (fileName == null || fileName.isEmpty()) {
				logger.error("Properties文件名不能为null或empty");
				return null;
			}
			// InputStream
			// in=Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
			InputStream in = new BufferedInputStream(new FileInputStream(
					System.getProperty("user.dir") + File.separator + "config" + File.separator + fileName));
			Properties properties = new Properties();
			properties.load(in);
			return properties;
		} catch (Exception e) {
			logger.error("读取Properties异常!", e);
			return null;
		}
	}
	
	/**
	 *  参数为要修改的文件路径  map存放要修改的属性名和属性值  
	 * @param fileName
	 * @param map
	 * @return
	 */
    public static Boolean updatePro(String fileName,Map<String,String> map){  
		Properties properties = new Properties();// 属性集合对象   
        InputStream in=null;
        String filePath=System.getProperty("user.dir") + File.separator + "config" + File.separator + fileName;
        try {  
        	in = new BufferedInputStream(new FileInputStream(filePath));
			properties.load(in);
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
            return false;  
        } catch (IOException e) {  
            e.printStackTrace();  
            return false;  
        }  
        for (String key : map.keySet()) {
        	 properties.setProperty(key, map.get(key));   
        }
       
        // 文件输出流   
        try {  
            FileOutputStream fos = new FileOutputStream(filePath);   
            // 将Properties集合保存到流中   
            properties.store(fos, "Copyright (c) Boxcode Studio");   
            fos.close();// 关闭流   
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
            return false;  
        } catch (IOException e) {  
            e.printStackTrace();  
            return false;  
        }  
//        System.out.println("获取修改后的属性值：monitor_time=" + properties.getProperty("monitor_time"));   
        return true;  
    }  
    /**
	 * 读取Properties某个值
	 * 
	 * @param fileName
	 * @return Properties
	 */
	public static String readByKey(String fileName,String key) {
		String result="";
		try {
			if (fileName == null || fileName.isEmpty()) {
				logger.error("Properties文件名不能为null或empty");
				return null;
			}
			// InputStream
			// in=Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
			InputStream in = new BufferedInputStream(new FileInputStream(
					System.getProperty("user.dir") + File.separator + "config" + File.separator + fileName));
			Properties properties = new Properties();
			properties.load(in);
			result=properties.getProperty(key);
		} catch (Exception e) {
			logger.error("读取Properties异常!", e);
		}
		return result;
	}
    
}
