package com.sckj.util;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public class DocColorUtil {
	
	/**
	 * 根据传入的颜色及文字，将文字插入文本域
	 * @param text
	 * @param textColor
	 */
	public static void insertDocument(JTextPane textPane,String text, Color textColor)// 根据传入的颜色及文字，将文字插入文本域
	{
		SimpleAttributeSet set = new SimpleAttributeSet();
		StyleConstants.setForeground(set, textColor);// 设置文字颜色
		StyleConstants.setFontSize(set, 18);// 设置字体大小
		Document doc = textPane.getStyledDocument();
		try {
			doc.insertString(doc.getLength(), text, set);// 插入文字
		} catch (BadLocationException e) {
		}
	}

}
