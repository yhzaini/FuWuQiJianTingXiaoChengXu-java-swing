package com.sckj.dto;

import java.util.List;

import com.sckj.base.BaseObject;

/**
 * 消息返回实体类
 * @author yanghz
 * @createDate 2017年10月27日
 */
public class ResultVo<T> extends BaseObject{

	private boolean rst;
	private String retCode;
	private String message;
	private Object obj;
	private List<Object> list;
	
	public ResultVo(){}

	public boolean isRst() {
		return rst;
	}

	public void setRst(boolean rst) {
		this.rst = rst;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public List<Object> getList() {
		return list;
	}

	public void setList(List<Object> list) {
		this.list = list;
	}
	
}
